package bean;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dao.AreaDAO;
import dao.PublicacaoDAO;
import dao.UsuarioDAO;
import model.Publicacao;
import model.Usuario;

@ManagedBean
@ViewScoped
public class PublicacaoBean {

	private FacesContext context = FacesContext.getCurrentInstance();
	private PublicacaoDAO publicacaoDao = new PublicacaoDAO();
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private Publicacao publicacao = new Publicacao();
	private Usuario usuario = ((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")) != null ? usuarioDAO.loadByEmail(((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")).getEmail()) : null;

	public Publicacao getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(Publicacao publicacao) {
		this.publicacao = publicacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public List<Publicacao> getPublicacoes() {
		return publicacaoDao.loadByUser(usuario);
	}

	public List<Publicacao> viewPublicacoes(Usuario usuario) {
		return publicacaoDao.loadByUser(usuario);
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	// MÉTODOS DE INTERAÇÃO COM O OBJETO PUBLICACAO
	
	public String getTitulo() {
		return this.publicacao.getTitulo();
	}

	public void setTitulo(String titulo) {
		this.publicacao.setTitulo(titulo);
	}

	public Date getDataPublicacao() {
		return this.publicacao.getDataPublicacao();
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.publicacao.setDataPublicacao(dataPublicacao);
	}

	public String getLinkPublicacao() {
		return this.publicacao.getLinkPublicacao();
	}

	public void setLinkPublicacao(String linkPublicacao) {
		this.publicacao.setLinkPublicacao(linkPublicacao);
	}
	
	// PERSISTÊNCIA DOS DADOS DO USUÁRIO

	public String add() {
		publicacao.setArea(new AreaDAO().loadAll().get(0));
		publicacao.setUsuario(usuario);
		publicacaoDao.persist(publicacao);
		return "dashboard?faces-redirect=true";
	}

	public void rm(Publicacao publicacao) {
		publicacaoDao.delete(publicacao);
	}

	public void up(Publicacao publicacao) {
		publicacaoDao.update(publicacao);
	}
}
