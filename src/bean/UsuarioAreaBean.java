package bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dao.UsuarioAreaDAO;
import dao.UsuarioDAO;
import model.Area;
import model.Usuario;
import model.UsuarioArea;

@ManagedBean
@ViewScoped
public class UsuarioAreaBean {
	
	private FacesContext context = FacesContext.getCurrentInstance();
	private UsuarioAreaDAO usuarioAreaDao = new UsuarioAreaDAO();
	private UsuarioArea usuarioArea = new UsuarioArea();
	private Usuario usuario = ((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")) != null ? new UsuarioDAO().loadByEmail(((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")).getEmail()) : null;
			
	
	public List<UsuarioArea> getUsuarioAreas() {
		return usuarioAreaDao.loadByUsuarioArea(usuario);
	}
			
	public void setUsuarioArea(Area area) {
		if(usuarioAreaDao.loadByUsuarioArea(usuario, area).size() == 0) {
			usuarioArea.setArea(area);
			usuarioArea.setUsuario(usuario);
		}
	}
	
	public UsuarioArea getUsuarioArea() {
		return usuarioArea;
	}
	
	//PERSISTÊNCIA DOS DADOS DA ÁREA DO USUÁRIO
	
	public void add() {
		System.out.println(usuarioArea.getArea().getNome());
		System.out.println(usuarioArea.getUsuario().getNome());
		System.out.println("aajiaia");
		usuarioAreaDao.persist(usuarioArea);
	}
	
	public void rm() {
		usuarioAreaDao.delete(usuarioArea);
	}
	
	public void up() {
		usuarioAreaDao.update(usuarioArea);
	}
}
