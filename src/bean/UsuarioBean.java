package bean;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dao.UsuarioDAO;
import model.Perfil;
import model.Usuario;

@ManagedBean
@ViewScoped
public class UsuarioBean {

	private UsuarioDAO usuarioDao = new UsuarioDAO();
	private Usuario usuario = new Usuario();
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	//MÉTODOS DE INTERAÇÃO COM O OBJETO USUÁRIO
	
	public String getNome() {
		return this.usuario.getNome();
	}
	
	public void setNome(String nome) {
		this.usuario.setNome(nome);
	}
	
	public String getSobrenome() {
		return this.usuario.getSobrenome();
	}
	
	public void setSobrenome(String sobrenome) {
		this.usuario.setSobrenome(sobrenome);
	}
	
	public String getCpf() {
		return this.usuario.getCpf();
	}
	
	public void setCpf(String cpf) {
		this.usuario.setCpf(cpf);
	}
	
	public String getEmail() {
		return this.usuario.getEmail();
	}
	
	public void setEmail(String email) {
		this.usuario.setEmail(email);
	}
	
	public String getSenha() {
		return this.usuario.getSenha();
	}
	
	public void setSenha(String senha) {
		this.usuario.setSenha(senha);
	}
	
	public Perfil getPerfil() {
		return this.usuario.getPerfil();
	}
	
	public void setPerfil(Perfil perfil) {
		this.usuario.setPerfil(perfil);
	}
	
	//PERSISTÊNCIA DOS DADOS DO USUÁRIO
	
	public String add() {
		usuarioDao.persist(usuario);
		return "dashboard?faces-redirect=true";
	}
	
	public void rm() {
		usuarioDao.delete(usuario);
	}
	
	public void up() {
		usuarioDao.update(usuario);
	}
}
