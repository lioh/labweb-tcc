package bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dao.PedidoOrientacaoDAO;
import dao.UsuarioDAO;
import model.PedidoOrientacao;
import model.Usuario;

@ManagedBean
@ViewScoped
public class PedidoOrientacaoBean {

	private FacesContext context = FacesContext.getCurrentInstance();
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private PedidoOrientacaoDAO pedidoOrientacaoDAO = new PedidoOrientacaoDAO();
	private Usuario usuarioLogado = ((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")) != null ? usuarioDAO.loadByEmail(((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")).getEmail()) : null;
	
	public void pedidoOrientacao(Usuario usuario) {
		pedidoOrientacaoDAO.pedidoOrientacao(usuarioLogado, usuario);
	}
	
	public List<Usuario> getRelacionamentos() {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		List<PedidoOrientacao> pedidos = pedidoOrientacaoDAO.loadRelacionamentosByUsuario(usuarioLogado);
		for (PedidoOrientacao pedidoOrientacao : pedidos) {
			if(usuarioLogado.getEmail().equals(pedidoOrientacao.getSolicitante().getEmail())) {
				usuarios.add(pedidoOrientacao.getSolicitado());
			} else {
				usuarios.add(pedidoOrientacao.getSolicitante());
			}
		}
		return usuarios;
	}
	
	public List<PedidoOrientacao> getPedidos() {
		return pedidoOrientacaoDAO.loadPedidosByUsuario(usuarioLogado);
	}
	
	public List<PedidoOrientacao> getSolicitacoes() {
		return pedidoOrientacaoDAO.loadSolicitacoesByUsuario(usuarioLogado);
	}
	
	public String aceitar(Usuario solicitante) {
		pedidoOrientacaoDAO.aceitar(solicitante, usuarioLogado, Boolean.TRUE);
		return "dashboard?faces-redirect=true";
	}
	
	public String recusar(Usuario solicitante) {
		pedidoOrientacaoDAO.aceitar(solicitante, usuarioLogado, Boolean.FALSE);
		return "dashboard?faces-redirect=true";
	}
	
}