package bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dao.AreaDAO;
import model.Area;

@ManagedBean
@ViewScoped
public class AreaBean {

	private AreaDAO areaDao = new AreaDAO();
	
	public List<Area> getAreas() {
		return areaDao.loadAll();
	}

}