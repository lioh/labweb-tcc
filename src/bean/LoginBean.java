package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dao.UsuarioDAO;
import model.Usuario;

@ManagedBean
@ViewScoped
public class LoginBean {
	
	private FacesContext context = FacesContext.getCurrentInstance();
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private Usuario usuario = new Usuario();
	private Usuario usuarioLogado = ((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")) != null ? usuarioDAO.loadByEmail(((Usuario) context.getExternalContext()
			.getSessionMap().get("usuarioLogado")).getEmail()) : null;
			
	public Usuario getUsuario() {
		return usuario;
	}
	
	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}
	
	public String login() {
		FacesContext context = FacesContext.getCurrentInstance();
		if(usuarioDAO.autenticar(usuario)) {
			 context.getExternalContext().getSessionMap()
             	.put("usuarioLogado", usuario);
			return "dashboard?faces-redirect=true";
		}
		return null;
	}
	
	public String logout() {
	    FacesContext context = FacesContext.getCurrentInstance();
	    context.getExternalContext().getSessionMap().remove("usuarioLogado");
	    return "login?faces-redirect=true";
	}
	
	public String register() {
	    return "usuario?faces-redirect=true";
	}
}
