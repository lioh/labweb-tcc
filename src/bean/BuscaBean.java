package bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dao.UsuarioDAO;
import model.Usuario;

@ManagedBean
@ViewScoped
public class BuscaBean {

	private FacesContext context = FacesContext.getCurrentInstance();
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private Usuario selectedUsuario;
	private Usuario usuarioLogado = ((Usuario) context.getExternalContext().getSessionMap()
			.get("usuarioLogado")) != null
					? usuarioDAO.loadByEmail(
							((Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado")).getEmail())
					: null;

	public void setSelectedUsuario(Usuario usuario) {
		selectedUsuario = usuario;
	}

	public Usuario getSelectedUsuario() {
		return selectedUsuario;
	}

	public List<Usuario> getUsuarios() {
		return usuarioDAO.filter(usuarioLogado);
	}

}
