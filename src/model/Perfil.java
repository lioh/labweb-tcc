package model;

public enum Perfil {
	
	Administrador("Administrador"), 
	Professor("Professor"), 
	Estudante("Estudante");
	
	private String perfil;
	
	Perfil(String perfil) {
		this.setPerfil(perfil);
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
}