package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PedidoOrientacao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(columnDefinition="bit(1)")
	private Boolean aceite = null;
	
	@ManyToOne
	private Usuario solicitante;
	
	@ManyToOne
	private Usuario solicitado;
	
	@Temporal(TemporalType.DATE)
	private Date dataSolicitacao;

	public Boolean isAceite() {
		return aceite;
	}

	public void setAceite(Boolean aceite) {
		this.aceite = aceite;
	}

	public Usuario getSolicitante() {
		return solicitante;
	}

	public Usuario getSolicitado() {
		return solicitado;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public void setSolicitante(Usuario solicitante) {
		this.solicitante = solicitante;
	}
	
	public void setSolicitado(Usuario solicitado) {
		this.solicitado = solicitado;
	}
	
	public void setDataSolicitacao(Date date) {
		this.dataSolicitacao = date;
	}
}