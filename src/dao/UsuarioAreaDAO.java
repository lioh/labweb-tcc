package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import model.Area;
import model.Universidade;
import model.Usuario;
import model.UsuarioArea;

public class UsuarioAreaDAO {
	private EntityManager entityManager = Persistence.createEntityManagerFactory("TccDB").createEntityManager();

	public UsuarioArea loadById(Long id) {
		return entityManager.find(UsuarioArea.class, id);
	}
	
	public void persist(UsuarioArea usuarioArea) {
		System.out.println("aqui");
		entityManager.getTransaction().begin();
		entityManager.persist(usuarioArea);
		entityManager.getTransaction().commit();
	}

	public void update(UsuarioArea area) {
		entityManager.getTransaction().begin();
		entityManager.merge(area);
		entityManager.getTransaction().commit();
	}

	public void delete(UsuarioArea area) {
		entityManager.getTransaction().begin();
		entityManager.remove(area);
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<Universidade> loadAll() {
		return entityManager.createQuery("select m from UsuarioArea m").getResultList();
	}
	
	public List<UsuarioArea> loadByUsuarioArea(Usuario usuario, Area area) {
		TypedQuery<UsuarioArea> query  = entityManager.createQuery("select m from UsuarioArea m"
				+ " where m.usuario= :pUsuario and m.area = :pArea", UsuarioArea.class);
		query.setParameter("pUsuario", usuario);
		query.setParameter("pArea", area);
		return query.getResultList();
	}
	
	public List<UsuarioArea> loadByUsuarioArea(Usuario usuario) {
		TypedQuery<UsuarioArea> query  = entityManager.createQuery("select m from UsuarioArea m"
				+ " where m.usuario= :pUsuario", UsuarioArea.class);
		query.setParameter("pUsuario", usuario);
		return query.getResultList();
	}

	public List<UsuarioArea> loadByUsuarioArea(Area area) {
		TypedQuery<UsuarioArea> query  = entityManager.createQuery("select m from UsuarioArea m"
				+ " where m.usuario= :pArea", UsuarioArea.class);
		query.setParameter("pArea", area);
		return query.getResultList();
	}
}
