package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import model.Publicacao;
import model.Usuario;

public class PublicacaoDAO {
	private EntityManager entityManager = Persistence.createEntityManagerFactory("TccDB").createEntityManager();

	public Publicacao loadById(Long id) {
		return entityManager.find(Publicacao.class, id);
	}

	public void persist(Publicacao publicacao) {
		entityManager.getTransaction().begin();
		entityManager.persist(publicacao);
		entityManager.getTransaction().commit();
	}

	public void update(Publicacao publicacao) {
		entityManager.getTransaction().begin();
		entityManager.merge(publicacao);
		entityManager.getTransaction().commit();
	}

	public void delete(Publicacao publicacao) {
		entityManager.getTransaction().begin();
		entityManager.remove(publicacao);
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<Publicacao> loadAll() {
		return entityManager.createQuery("select m from Publicacao m order by m.titulo").getResultList();
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<Publicacao> loadByUser(Usuario usuario) {
		if(usuario != null ) {
			Session session = (Session) entityManager.getDelegate();
			Criteria criteria = session.createCriteria(Publicacao.class);
			criteria.add(Restrictions.eq("usuario",usuario));
			return criteria.list();
		}
		return null;
	}

}