package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import model.Universidade;

public class UniversidadeDAO {

	private EntityManager entityManager = Persistence.createEntityManagerFactory("TccDB").createEntityManager();

	public Universidade loadById(Long id) {
		return entityManager.find(Universidade.class, id);
	}

	public void persist(Universidade universidade) {
		entityManager.getTransaction().begin();
		entityManager.persist(universidade);
		entityManager.getTransaction().commit();
	}

	public void update(Universidade universidade) {
		entityManager.getTransaction().begin();
		entityManager.merge(universidade);
		entityManager.getTransaction().commit();
	}

	public void delete(Universidade universidade) {
		entityManager.getTransaction().begin();
		entityManager.remove(universidade);
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<Universidade> loadAll() {
		return entityManager.createQuery("select m from Area m order by m.nome").getResultList();
	}
}