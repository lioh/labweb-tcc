package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import model.PedidoOrientacao;
import model.Usuario;

public class UsuarioDAO {

	private EntityManager entityManager = Persistence.createEntityManagerFactory("TccDB").createEntityManager();

	public Usuario loadById(Long id) {
		return entityManager.find(Usuario.class, id);
	}

	public Usuario loadByEmail(String email) {
		TypedQuery<Usuario> query = entityManager
				.createQuery("select m from Usuario m where m.email = :pEmail", Usuario.class);
		query.setParameter("pEmail", email);
		return query.getSingleResult();
	}
	
	public void persist(Usuario usuario) {
		if(!entityManager.getTransaction().isActive())
			entityManager.getTransaction().begin();
		entityManager.persist(usuario);
		entityManager.getTransaction().commit();
	}

	public void update(Usuario usuario) {
		entityManager.getTransaction().begin();
		entityManager.merge(usuario);
		entityManager.getTransaction().commit();
	}

	public void delete(Usuario usuario) {
		entityManager.getTransaction().begin();
		entityManager.remove(usuario);
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> loadAll() {
		return entityManager.createQuery("select m from Usuario m order by m.nome").getResultList();
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Usuario> filter(Usuario usuario) {
		Disjunction orSolicitantes = Restrictions.disjunction();
		Disjunction orAceite = Restrictions.disjunction();
		Session session = (Session) entityManager.getDelegate();
		Criteria criteria = session.createCriteria(Usuario.class);
		Criteria criteriaAuxiliar = session.createCriteria(PedidoOrientacao.class);
		//NÃO LISTA JÁ SOLICITADOS
		orSolicitantes.add(Restrictions.eq("solicitante", usuario));
		orSolicitantes.add(Restrictions.eq("solicitado", usuario));
		orAceite.add(Restrictions.isNull("aceite"));
		orAceite.add(Restrictions.eq("aceite", Boolean.TRUE));
		criteriaAuxiliar.add(orSolicitantes);
		criteriaAuxiliar.add(orAceite);
		List<PedidoOrientacao> pedidos = criteriaAuxiliar.list();
		List<Long> listids = new ArrayList<Long>();
		for (PedidoOrientacao pedido : pedidos) {
			listids.add(pedido.getSolicitado().getId());
			listids.add(pedido.getSolicitante().getId());
		}
		criteria.add(Restrictions.not(Restrictions.eq("perfil",usuario.getPerfil())));
		if (listids != null && !listids.isEmpty())
		    criteria.add(Restrictions.not(Restrictions.in("id", listids)));
		return criteria.list();
	}
	
	public boolean autenticar(Usuario usuario) {

		TypedQuery<Usuario> query = entityManager
				.createQuery("select m from Usuario m where (m.email = :pEmail or m.cpf = :pCpf ) and m.senha = :pSenha", Usuario.class);
		
		query.setParameter("pEmail", (usuario.getEmail() != null ? usuario.getEmail() : ""));
		query.setParameter("pCpf", (usuario.getCpf() != null ? usuario.getCpf() : ""));
		query.setParameter("pSenha", usuario.getSenha());

		try {			
			query.getSingleResult();
		} catch (Exception e) {
			return false;
		}

		entityManager.close();
		return true;
	}

}
