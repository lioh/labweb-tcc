package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import model.Area;

public class AreaDAO {
	private EntityManager entityManager = Persistence.createEntityManagerFactory("TccDB").createEntityManager();

	public Area loadById(Long id) {
		return entityManager.find(Area.class, id);
	}

	public void persist(Area area) {
		entityManager.getTransaction().begin();
		entityManager.persist(area);
		entityManager.getTransaction().commit();
	}

	public void update(Area area) {
		entityManager.getTransaction().begin();
		entityManager.merge(area);
		entityManager.getTransaction().commit();
	}

	public void delete(Area area) {
		entityManager.getTransaction().begin();
		entityManager.remove(area);
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<Area> loadAll() {
		return entityManager.createQuery("select m from Area m order by m.nome").getResultList();
	}
}
