package dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import model.PedidoOrientacao;
import model.Usuario;

public class PedidoOrientacaoDAO {

	private EntityManager entityManager = Persistence.createEntityManagerFactory("TccDB").createEntityManager();

	public PedidoOrientacao loadById(Long id) {
		return entityManager.find(PedidoOrientacao.class, id);
	}
	
	public void persist(PedidoOrientacao pedidoOrientacao) {
		entityManager.getTransaction().begin();
		entityManager.persist(pedidoOrientacao);
		entityManager.getTransaction().commit();
	}

	public void update(PedidoOrientacao pedidoOrientacao) {
		entityManager.getTransaction().begin();
		entityManager.merge(pedidoOrientacao);
		entityManager.getTransaction().commit();
	}
	
	public void delete(PedidoOrientacao pedidoOrientacao) {
		entityManager.getTransaction().begin();
		entityManager.remove(pedidoOrientacao);
		entityManager.getTransaction().commit();
	}
	
	public void pedidoOrientacao(Usuario usuarioLogado, Usuario usuario) {
		if(this.loadBySolicitanteSolicitado(usuarioLogado, usuario).size() == 0) {
			PedidoOrientacao pedidoOrientacao = new PedidoOrientacao();
			pedidoOrientacao.setSolicitante(usuarioLogado);
			pedidoOrientacao.setSolicitado(usuario);
			pedidoOrientacao.setDataSolicitacao(new Date());
			this.persist(pedidoOrientacao);
		}
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<PedidoOrientacao> loadBySolicitanteSolicitado(Usuario solicitante, Usuario solicitado) {
		if(solicitante != null && solicitado != null) {
			Session session = (Session) entityManager.getDelegate();
			Criteria criteria = session.createCriteria(PedidoOrientacao.class);
			criteria.add(Restrictions.eq("solicitante", solicitante));
			criteria.add(Restrictions.eq("solicitado", solicitado));
			criteria.addOrder(Order.desc("dataSolicitacao"));
			return criteria.list();
		}
		return null;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<PedidoOrientacao> loadRelacionamentosByUsuario(Usuario usuario) {
		if(usuario != null) {
			Disjunction or = Restrictions.disjunction();
			Session session = (Session) entityManager.getDelegate();
			Criteria criteria = session.createCriteria(PedidoOrientacao.class);
			or.add(Restrictions.eq("solicitante", usuario));
			or.add(Restrictions.eq("solicitado", usuario));
			criteria.add(or);
			criteria.add(Restrictions.eq("aceite", true));
			return criteria.list();
		}
		return null;
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<PedidoOrientacao> loadPedidosByUsuario(Usuario usuario) {
		if(usuario != null) {
			Session session = (Session) entityManager.getDelegate();
			Criteria criteria = session.createCriteria(PedidoOrientacao.class);
			criteria.add(Restrictions.eq("solicitante", usuario));
			criteria.add(Restrictions.isNull("aceite"));
			return criteria.list();
		}
		return null;
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<PedidoOrientacao> loadSolicitacoesByUsuario(Usuario usuario) {
		if(usuario != null) {
			Session session = (Session) entityManager.getDelegate();
			Criteria criteria = session.createCriteria(PedidoOrientacao.class);
			criteria.add(Restrictions.eq("solicitado", usuario));
			criteria.add(Restrictions.isNull("aceite"));
			return criteria.list();
		}
		return null;
	}

	public void aceitar(Usuario solicitante, Usuario solicitado, Boolean aceitar) {
		List<PedidoOrientacao> list = this.loadBySolicitanteSolicitado(solicitante, solicitado);
		PedidoOrientacao pedidoOrientacao = list.get(0);
		if(pedidoOrientacao.isAceite() == null) {
			pedidoOrientacao.setAceite(aceitar);
		}
		this.update(pedidoOrientacao);
	}
}